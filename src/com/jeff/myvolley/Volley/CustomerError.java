package com.jeff.myvolley.Volley;

@SuppressWarnings("serial")
public class CustomerError extends VolleyError{
    private int errorCode; 
    private int errorMsg;
    public CustomerError(int  ErrorCode){
	super(String.valueOf(ErrorCode));
//	switch (ErrorCode) {
//	case 1:
//	    
//	    break;
//
//	default:
//	    break;
//	}
	errorCode = ErrorCode;
	
    }
    
    public CustomerError(int ErrorCode , String errorMsg){
	super(String.valueOf(ErrorCode));
    }

    public int getErrorCode() {
	return errorCode;
    }

    public void setErrorCode(int errorCode) {
	this.errorCode = errorCode;
    }

    public int getErrorMsg() {
	return errorMsg;
    }

    public void setErrorMsg(int errorMsg) {
	this.errorMsg = errorMsg;
    }
}