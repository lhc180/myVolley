package com.jeff.myvolley.Volley.toolbox;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.jeff.myvolley.Volley.DefaultRetryPolicy;
import com.jeff.myvolley.Volley.NetworkResponse;
import com.jeff.myvolley.Volley.Request;
import com.jeff.myvolley.Volley.Response;

public class FileRequest extends Request<String> {


    private final Response.Listener<String> mListener;

    /** Socket timeout in milliseconds for image requests */
    private static final int IMAGE_TIMEOUT_MS = 1000;

    /** Default number of retries for image requests */
    private static final int IMAGE_MAX_RETRIES = 2;

    /** Default backoff multiplier for image requests */
    private static final float IMAGE_BACKOFF_MULT = 2f;
    public FileRequest(String url, Response.ErrorListener errorListener, Response.Listener<String> listener)
    {
        super(Method.GET, url, errorListener);
        setRetryPolicy(
                new DefaultRetryPolicy(IMAGE_TIMEOUT_MS, IMAGE_MAX_RETRIES, IMAGE_BACKOFF_MULT));
        mListener = listener;

       // buildMultipartEntity(fileName);
    }

//    private void buildMultipartEntity(String fileName)
//    {
////    	entity.addPart(name, contentBody)
//        entity.addPart(fileName, new FileBody(mFilePart));
//        try
//        {
//        	Iterator<String> iterator = mStringPart.keys();
//        	while (iterator.hasNext()) 
//        	{
//				String next = iterator.next();
//				try {
//					entity.addPart(next, new StringBody(mStringPart.getString(next),Charset.forName("UTF-8")));
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				
//			}
//            
//        }
//        catch (UnsupportedEncodingException e)
//        {
//            VolleyLog.e("UnsupportedEncodingException");
//        }
//    }

//    @Override
//    public String getBodyContentType()
//    {
//        return "application/octet-stream";
//    }
//
//    @Override
//    public byte[] getBody() throws AuthFailureError
//    {
//        return null;
//    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response)
    {
    	byte[] data = response.data;
    	ByteArrayInputStream  bis = new ByteArrayInputStream(data);
    	File file = new File("mnt/sdcard/myFile.apk");  
    	FileOutputStream fos = null;
		try {
			fos = new  FileOutputStream(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {
			fos.write(data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return Response.success("success", getCacheEntry());
//    	String parsed;
//        try {
//            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
//        } catch (UnsupportedEncodingException e) {
//            parsed = new String(response.data);
//        }
//        return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected void deliverResponse(String response)
    {
        mListener.onResponse(response);
    }

//	@Override
//	protected void deliverResponse(Strings response) {
//		// TODO Auto-generated method stub
//		
//	}
}